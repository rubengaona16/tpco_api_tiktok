﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tpco_TikTokApi.Models.Request
{
    public class UserResponse
    {
        public string User { get; set; }
        public string Token { get; set; }
    }
}
