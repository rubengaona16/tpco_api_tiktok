﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tpco_TikTokApi.Models.Request;

namespace tpco_TikTokApi.Services
{
    public interface IUserServices
    {
        UserResponse Auth(AuthRequest model);
    }
}
