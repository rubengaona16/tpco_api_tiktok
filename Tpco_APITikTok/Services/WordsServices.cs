﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TP_DB_Connection.Data.SqlServer;
using Tpco_APITikTok.Model.Request;
using Tpco_APITikTok.Model.Response;

namespace Tpco_APITikTok.Services
{
    public class WordsServices : IWordsServices
    {

        public List<ResponseWords> word(List<string> words)
        {

            try
            {
                var Data = new ObjectWords();
                var ListWords = new List<ResponseWords>();
                foreach (var lstwords in words)
                {
                    string[] Words = lstwords.ToString().Split(" ");
                    //var wordRequest = lstwords.ToString().ToLower();
                    foreach (var wordsSplit in Words)
                    {
                        DbBase db_ = new DbBase(@"Data Source=TPCCD-DB37.teleperformance.co\SQL2016DEV,5081;Initial Catalog=dbTiktok;Integrated Security=True");
                        var res = db_.SerchList<ResponseWords>(new { word = wordsSplit }, "spWordValidate");
                        if (res != null)
                        {
                            foreach (var lst in res)
                            {
                                ListWords.Add(new ResponseWords
                                {
                                    //id = lst.id,
                                    word = lst.word,
                                    politic = lst.politic,
                                    color = lst.color,
                                    statusw = lst.statusw
                                });
                            }
                        }
                        else
                        {

                        }
                        Data.ResponseWords = ListWords;

                    }

                }
                return Data.ResponseWords;

            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
