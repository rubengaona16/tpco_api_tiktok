﻿using Microsoft.CodeAnalysis.Options;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using tpco_TikTokApi.Models.Helpers;
using tpco_TikTokApi.Models.Request;
using TP_DB_Connection.Data.SqlServer;
using WSValidationUser;
//using wsValidationUser;

namespace tpco_TikTokApi.Services
{
    public class UserServices : IUserServices
    {
        public class User
        {
            public string username { get; set; }

        }
        private readonly AppSettings _appSettings;
        

        public UserServices(IOptions<AppSettings> appSetting)
        {
            _appSettings = appSetting.Value;           
        }
        public UserResponse Auth(AuthRequest model)
        {
            UserResponse userResponse = new UserResponse();
            //DbBase db_ = new DbBase(dbConfig.sConnection);
            DbBase db_ = new DbBase(@"Data Source=TPCCD-DB37.teleperformance.co\SQL2016DEV,5081;Initial Catalog=dbTiktok;Integrated Security=True");
            var res = db_.SerchList<User>(new { user = model.Username }, "spSessionUserNameGet");

            UserSoapClient cliL = new UserSoapClient(new UserSoapClient.EndpointConfiguration());


            //validacion a web services
            if (res != null)
            {
                var auR = cliL.AuthenticateUser(model.Username, model.Password);
                if (auR)
                    userResponse.Token=GetToken(model).ToString();



            }
            return userResponse;
        }


        private string GetToken(AuthRequest model)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secreto);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new System.Security.Claims.ClaimsIdentity(
                    new Claim[]
                    {
                        new Claim(ClaimTypes.Name, model.Username)
                    }
                    ),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var Token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(Token);
        }
    }
}
