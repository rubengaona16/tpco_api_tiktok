﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tpco_APITikTok.Model.Request;
using Tpco_APITikTok.Model.Response;

namespace Tpco_APITikTok.Services
{
    public interface IWordsServices
    {
        //List<ResponseWords> words(List<WordRequest> words);
        //List<ResponseWords> word(string words);
        List<ResponseWords> word(List<string> words);
    }
}
