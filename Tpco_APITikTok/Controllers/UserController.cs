﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tpco_TikTokApi.Models.Request;
using tpco_TikTokApi.Models.Response;
using tpco_TikTokApi.Services;

namespace tpco_TikTokApi.Controllers
{
    public class UserController : Controller
    {
        private IUserServices _userServices;

        public UserController(IUserServices userServices)
        {
            _userServices = userServices;
        }

        [HttpPost("Login")]
        public IActionResult Authetication([FromBody] AuthRequest user)
        {
            ResponseUser responseUser = new ResponseUser();
            var userResponse = _userServices.Auth(user);
            if (userResponse == null)
            {
                responseUser.Message = "Usuario no encontrado";
                return BadRequest(responseUser);
            }

            return Ok(userResponse.Token);
        }

       
    }
}
