﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tpco_APITikTok.Model.Request;
using Tpco_APITikTok.Model.Response;
using Tpco_APITikTok.Services;

namespace Tpco_APITikTok.Controllers
{
    //[Authorize]
    public class CounterWordsController : Controller
    {
        private IWordsServices _wordsServices;
        public CounterWordsController(IWordsServices wordsServices)
        {
            _wordsServices = wordsServices;
        }

        [HttpPost("wordInsult")]
        public IActionResult WordsValidate([FromBody] ValidateWords Text)
        {
            var text = Text.Text;
            var responseWords = new List<ResponseWords>();
            List<string> list = new List<string>();
            list = text.Split(',').ToList();
            if (list != null)
            {
                var request = _wordsServices.word(list);
                responseWords = request;
                return Ok(responseWords);

            }
            
            else
            {
                return BadRequest();
            }

        }
    }
}
